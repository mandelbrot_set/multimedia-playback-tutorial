#include <stdio.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/pixdesc.h>


void print_title_and_language(AVStream* stream) {
	AVDictionaryEntry* title = NULL;
	AVDictionaryEntry* language = NULL;

	if (!stream->metadata) {
		return;
	}

	title = av_dict_get(stream->metadata, "title", NULL, 0);
	language = av_dict_get(stream->metadata, "language", NULL, 0);

	if (title && title->value) {
		printf(" \"%s\"", title->value);
	}

	if (language && language->value) {
		printf(" (%s)", language->value);
	}
}


void dump_streams_info(AVFormatContext* format_context) {
	for (int stream_index = 0; stream_index < format_context->nb_streams; ++stream_index) {
		AVStream* stream = format_context->streams[stream_index];
		AVCodecContext* codec_context = stream->codec;
		AVCodec* codec = avcodec_find_decoder(codec_context->codec_id);

		switch (codec_context->codec_type) {
			case AVMEDIA_TYPE_VIDEO:
				printf("#%d: video:", stream_index);
				print_title_and_language(stream);
				printf(" - %dx%d, %0.2f fps, %s, %s\n",
					codec_context->width, codec_context->height,
					av_q2d(stream->avg_frame_rate),
					codec->name,
					av_get_pix_fmt_name(codec_context->pix_fmt));
				break;
			case AVMEDIA_TYPE_AUDIO:
				printf("#%d: audio:", stream_index);
				print_title_and_language(stream);
				printf(" - %d channels, %d Hz, %s, %d kb/s\n",
					codec_context->channels,
					(int)codec_context->sample_rate,
					codec->name,
					codec_context->bit_rate / 1000);
				break;
			default:
				break;
		}
	}
}


int main(int argc, char** argv) {
	int exit_code = 0;
	AVFormatContext* format_context = NULL;

	if (argc < 2) {
		fprintf(stderr, "Please provide a multimedia file\n");
		exit_code = -1;
		goto terminate;
	}

	av_register_all();

	if (avformat_open_input(&format_context, argv[1], NULL, NULL) != 0) {
		fprintf(stderr, "Failed to open input file\n");
		exit_code = -1;
		goto terminate;
	}

	if (avformat_find_stream_info(format_context, NULL) < 0) {
		fprintf(stderr, "Failed to find stream information\n");
		exit_code = -1;
		goto close_input;
	}

	dump_streams_info(format_context);

close_input:
	avformat_close_input(&format_context);
terminate:
	return exit_code;
}
