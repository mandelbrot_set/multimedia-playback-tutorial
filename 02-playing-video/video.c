#include <stdio.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/pixdesc.h>

#include <SDL.h>


void print_title_and_language(AVStream* stream) {
	AVDictionaryEntry* title = NULL;
	AVDictionaryEntry* language = NULL;

	if (!stream->metadata) {
		return;
	}

	title = av_dict_get(stream->metadata, "title", NULL, 0);
	language = av_dict_get(stream->metadata, "language", NULL, 0);

	if (title && title->value) {
		printf(" \"%s\"", title->value);
	}

	if (language && language->value) {
		printf(" (%s)", language->value);
	}
}


void dump_streams_info(AVFormatContext* format_context) {
	for (int stream_index = 0; stream_index < format_context->nb_streams; ++stream_index) {
		AVStream* stream = format_context->streams[stream_index];
		AVCodecContext* codec_context = stream->codec;
		AVCodec* codec = avcodec_find_decoder(codec_context->codec_id);

		switch (codec_context->codec_type) {
			case AVMEDIA_TYPE_VIDEO:
				printf("#%d: video:", stream_index);
				print_title_and_language(stream);
				printf(" - %dx%d, %0.2f fps, %s, %s\n",
					codec_context->width, codec_context->height,
					av_q2d(stream->avg_frame_rate),
					codec->name,
					av_get_pix_fmt_name(codec_context->pix_fmt));
				break;
			case AVMEDIA_TYPE_AUDIO:
				printf("#%d: audio:", stream_index);
				print_title_and_language(stream);
				printf(" - %d channels, %d Hz, %s, %d kb/s\n",
					codec_context->channels,
					(int)codec_context->sample_rate,
					codec->name,
					codec_context->bit_rate / 1000);
				break;
			default:
				break;
		}
	}
}


int get_video_stream_index(AVFormatContext* format_context) {
	for (int stream_index = 0; stream_index < format_context->nb_streams; ++stream_index) {
		AVCodecContext* codec_context = format_context->streams[stream_index]->codec;
		if (codec_context->codec_type == AVMEDIA_TYPE_VIDEO) {
			return stream_index;
		}
	}

	return -1;
}


void main_loop(AVFormatContext* format_context,
				int video_stream_index,
				AVCodecContext* codec_context,
				AVFrame* frame,
				SDL_Overlay* overlay,
				struct SwsContext* sws_context)
{
	int end_of_file_reached = 0;
	int frame_decoded;
	AVPacket packet;

	int video_width = codec_context->width;
	int video_height = codec_context->height;

	SDL_Rect rect = {.x = 0, .y = 0, .w = video_width, .h = video_height};
	SDL_Event event;

	uint8_t* data[3];
	int linesize[3];

	data[0] = overlay->pixels[0];
	data[1] = overlay->pixels[2];
	data[2] = overlay->pixels[1];

	linesize[0] = overlay->pitches[0];
	linesize[1] = overlay->pitches[2];
	linesize[2] = overlay->pitches[1];

	while (!end_of_file_reached || frame_decoded) {

		if (av_read_frame(format_context, &packet) < 0) {
			end_of_file_reached = 1;
			packet.data = NULL;
			packet.size = 0;
			packet.stream_index = video_stream_index;
		}

		if (packet.stream_index == video_stream_index) {
			avcodec_decode_video2(codec_context, frame, &frame_decoded, &packet);

			if (frame_decoded) {
				SDL_LockYUVOverlay(overlay);

				sws_scale(sws_context, (const uint8_t * const*)frame->data, frame->linesize, 0, video_height, data, linesize);

				SDL_UnlockYUVOverlay(overlay);

				SDL_DisplayYUVOverlay(overlay, &rect);

				// SDL_Delay(40);
			}
		}

		if (!end_of_file_reached) {
			av_free_packet(&packet);
		}

		while (SDL_PollEvent(&event)) {
			switch (event.type) {
				case SDL_QUIT:
					return;
				default:
					break;
			}
		}
	}
}


int main(int argc, char** argv) {
	int exit_code = 0;

	AVFormatContext* format_context = NULL;
	AVCodecContext* codec_context;
	AVCodec* codec;
	AVDictionary* codec_options = NULL;
	AVFrame* frame;

	int video_stream_index;
	int video_width;
	int video_height;

	SDL_Surface* surface;
	SDL_Overlay* overlay;

	struct SwsContext* sws_context;

	if (argc < 2) {
		fprintf(stderr, "Please provide a multimedia file\n");
		exit_code = -1;
		goto terminate;
	}

	av_register_all();

	if (avformat_open_input(&format_context, argv[1], NULL, NULL) != 0) {
		fprintf(stderr, "Failed to open input file\n");
		exit_code = -1;
		goto terminate;
	}

	if (avformat_find_stream_info(format_context, NULL) < 0) {
		fprintf(stderr, "Failed to find stream information\n");
		exit_code = -1;
		goto close_input;
	}

	video_stream_index = get_video_stream_index(format_context);
	if (video_stream_index < 0) {
		fprintf(stderr, "No video streams found\n");
		exit_code = -1;
		goto close_input;
	}
	codec_context = format_context->streams[video_stream_index]->codec;

	video_width = codec_context->width;
	video_height = codec_context->height;

	dump_streams_info(format_context);

	codec = avcodec_find_decoder(codec_context->codec_id);
	if (!codec) {
		fprintf(stderr, "Unsupported video codec\n");
		exit_code = -1;
		goto close_input;
	}

	if (avcodec_open2(codec_context, codec, &codec_options) < 0) {
		fprintf(stderr, "Failed to open codec\n");
		exit_code = -1;
		goto close_input;
	}

	frame = avcodec_alloc_frame();
	if (!frame) {
		fprintf(stderr, "Failed to allocate video frame\n");
		exit_code = -1;
		goto close_codec;
	}

	if (SDL_Init(SDL_INIT_VIDEO)) {
		fprintf(stderr, "Failed to initialize SDL: %s\n", SDL_GetError());
		exit_code = -1;
		goto free_frame;
	}

	surface = SDL_SetVideoMode(video_width, video_height, 0, 0);
	if (!surface) {
		fprintf(stderr, "Failed to set video mode: %s\n", SDL_GetError());
		exit_code = -1;
		goto close_sdl;
	}

	overlay = SDL_CreateYUVOverlay(video_width, video_height, SDL_YV12_OVERLAY, surface);
	if (!overlay) {
		fprintf(stderr, "Failed to create YUV overlay: %s\n", SDL_GetError());
		exit_code = -1;
		goto free_surface;
	}

	sws_context = sws_getContext(video_width, video_height, codec_context->pix_fmt, video_width, video_height, PIX_FMT_YUV420P, SWS_BILINEAR, NULL, NULL, NULL);
	if (!sws_context) {
		fprintf(stderr, "Failed to create swscale context\n");
		exit_code = -1;
		goto free_overlay;
	}

	main_loop(format_context, video_stream_index, codec_context, frame, overlay, sws_context);

	sws_freeContext(sws_context);
free_overlay:
	SDL_FreeYUVOverlay(overlay);
free_surface:
	SDL_FreeSurface(surface);
close_sdl:
	SDL_Quit();
free_frame:
	avcodec_free_frame(&frame);
close_codec:
	avcodec_close(codec_context);
close_input:
	avformat_close_input(&format_context);
terminate:
	return exit_code;
}
