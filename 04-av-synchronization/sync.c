#include <stdio.h>
#include <malloc.h>
#include <math.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/avutil.h>
#include <libavutil/pixdesc.h>

#include <SDL.h>


#define AUDIO_BUFFER_SIZE (AVCODEC_MAX_AUDIO_FRAME_SIZE * 3) / 2
#define SDL_AUDIO_BUFFER_SIZE 1024


typedef struct demuxer_stream {
	AVPacketList* first;
	AVPacketList* last;
} DemuxerStream;


typedef struct audio_buffer {
	uint8_t buffer[AUDIO_BUFFER_SIZE];
	int data_size;
	int capacity;
} AudioBuffer;


typedef struct player {
	AVFormatContext* format_context;

	int video_stream_index;
	int audio_stream_index;

	AVCodecContext* video_codec_context;
	AVCodecContext* audio_codec_context;
	AVCodec* video_codec;
	AVCodec* audio_codec;
	AVDictionary* video_codec_options;
	AVDictionary* audio_codec_options;

	AVFrame* video_frame;
	AVFrame* audio_frame;

	SDL_Surface* surface;
	SDL_Overlay* overlay;

	SDL_mutex* audio_mutex;
	SDL_cond* audio_cond;

	int out_channels;
	enum AVSampleFormat out_sample_fmt;
	uint64_t out_channel_layout;
	int out_sample_rate;
	int bytes_per_sample;
	int bytes_per_second;

	struct SwsContext* sws_context;
	SwrContext* swr_context;

	DemuxerStream audio_demuxer_stream;
	DemuxerStream video_demuxer_stream;

	AudioBuffer audio_buffer;

	double audio_clock;
	double video_clock;

	double last_display_time;
	int64_t last_timestamp;

	int is_running;
	int end_of_file_reached;
	int video_decoding_finished;
} Player;


void demuxer_stream_flush(DemuxerStream* demuxer_stream) {
	while (demuxer_stream->first) {
		AVPacketList* item = demuxer_stream->first;
		demuxer_stream->first = item->next;

		av_free_packet(&item->pkt);
		free(item);
	}

	demuxer_stream->last = NULL;
}


int demuxer_stream_put_packet(DemuxerStream* demuxer_stream, AVPacket* packet) {
	AVPacketList* item = av_malloc(sizeof(AVPacketList));

	if (!item) {
		return -1;
	}

	item->pkt = *packet;
	item->next = NULL;
	av_dup_packet(&item->pkt);

	if (demuxer_stream->last) {
		demuxer_stream->last->next = item;
	} else {
		demuxer_stream->first = item;
	}
	demuxer_stream->last = item;

	return 0;
}


int demuxer_stream_get_packet(DemuxerStream* demuxer_stream, AVPacket* packet) {
	AVPacketList* item = demuxer_stream->first;

	if (!item) {
		return -1;
	}

	demuxer_stream->first = demuxer_stream->first->next;
	if (demuxer_stream->first == NULL) {
		demuxer_stream->last = NULL;
	}

	*packet = item->pkt;
	av_free(item);

	return 0;
}


void audio_buffer_update_after_read(AudioBuffer* audio_buffer, int number_of_bytes) {
	audio_buffer->data_size -= number_of_bytes;
	memmove(audio_buffer->buffer, audio_buffer->buffer + number_of_bytes, audio_buffer->data_size);
}


void audio_buffer_update_after_write(AudioBuffer* audio_buffer, int number_of_bytes) {
	audio_buffer->data_size += number_of_bytes;
}


Player* new_player(void) {
	Player* player = malloc(sizeof(Player));

	if (!player) {
		goto failure;
	}

	player->audio_mutex = SDL_CreateMutex();
	if (!player->audio_mutex) {
		goto free_player;
	}
	player->audio_cond = SDL_CreateCond();
	if (!player->audio_cond) {
		goto free_mutex;
	}

	player->audio_buffer.capacity = AUDIO_BUFFER_SIZE;

	return player;

free_mutex:
	SDL_DestroyMutex(player->audio_mutex);
free_player:
	free(player);
failure:
	return NULL;
}


void free_player(Player* player) {
	demuxer_stream_flush(&player->audio_demuxer_stream);
	SDL_DestroyMutex(player->audio_mutex);
	SDL_DestroyCond(player->audio_cond);
	free(player);
}


void print_title_and_language(AVStream* stream) {
	AVDictionaryEntry* title = NULL;
	AVDictionaryEntry* language = NULL;

	if (!stream->metadata) {
		return;
	}

	title = av_dict_get(stream->metadata, "title", NULL, 0);
	language = av_dict_get(stream->metadata, "language", NULL, 0);

	if (title && title->value) {
		printf(" \"%s\"", title->value);
	}

	if (language && language->value) {
		printf(" (%s)", language->value);
	}
}


void dump_streams_info(AVFormatContext* format_context) {
	for (int stream_index = 0; stream_index < format_context->nb_streams; ++stream_index) {
		AVStream* stream = format_context->streams[stream_index];
		AVCodecContext* codec_context = stream->codec;
		AVCodec* codec = avcodec_find_decoder(codec_context->codec_id);

		switch (codec_context->codec_type) {
			case AVMEDIA_TYPE_VIDEO:
				printf("#%d: video:", stream_index);
				print_title_and_language(stream);
				printf(" - %dx%d, %0.2f fps, %s, %s\n",
					codec_context->width, codec_context->height,
					av_q2d(stream->avg_frame_rate),
					codec->name,
					av_get_pix_fmt_name(codec_context->pix_fmt));
				break;
			case AVMEDIA_TYPE_AUDIO:
				printf("#%d: audio:", stream_index);
				print_title_and_language(stream);
				printf(" - %d channels, %d Hz, %s, %d kb/s\n",
					codec_context->channels,
					(int)codec_context->sample_rate,
					codec->name,
					codec_context->bit_rate / 1000);
				break;
			default:
				break;
		}
	}
}


int get_stream_index_for_media_type(AVFormatContext* format_context, enum AVMediaType media_type) {
	for (int stream_index = 0; stream_index < format_context->nb_streams; ++stream_index) {
		AVCodecContext* codec_context = format_context->streams[stream_index]->codec;
		if (codec_context->codec_type == media_type) {
			return stream_index;
		}
	}

	return -1;
}


int get_video_stream_index(AVFormatContext* format_context) {
	return get_stream_index_for_media_type(format_context, AVMEDIA_TYPE_VIDEO);
}


int get_audio_stream_index(AVFormatContext* format_context) {
	return get_stream_index_for_media_type(format_context, AVMEDIA_TYPE_AUDIO);
}


int demux(Player* player) {
	AVPacket packet;

	if (av_read_frame(player->format_context, &packet) < 0) {
		player->end_of_file_reached = 1;
		return -1;
	}

	if (packet.stream_index == player->audio_stream_index) {
		demuxer_stream_put_packet(&player->audio_demuxer_stream, &packet);
	} else {
		if (packet.stream_index == player->video_stream_index) {
			demuxer_stream_put_packet(&player->video_demuxer_stream, &packet);
		} else {
			av_free_packet(&packet);
		}
	}

	return 0;
}


int get_demuxer_stream_packet(Player* player, AVPacket* packet, DemuxerStream* demuxer_stream) {
	while (!demuxer_stream->first) {
		if (demux(player) < 0) {
			return -1;
		}
	}

	demuxer_stream_get_packet(demuxer_stream, packet);

	return 0;
}


int get_audio_packet(Player* player, AVPacket* packet) {
	return get_demuxer_stream_packet(player, packet, &player->audio_demuxer_stream);
}


int get_video_packet(Player* player, AVPacket* packet) {
	return get_demuxer_stream_packet(player, packet, &player->video_demuxer_stream);
}


int decode_audio(Player* player, AVPacket* packet, int* decoded_size) {
	AVCodecContext* codec_context = player->audio_codec_context;
	AVFrame* frame = player->audio_frame;
	AudioBuffer* audio_buffer = &player->audio_buffer;

	int frame_decoded = 0;
	int data_size;

	*decoded_size = 0;
	data_size = avcodec_decode_audio4(codec_context, frame, &frame_decoded, packet);
	if (data_size < 0) {
		return -1;
	}

	if (frame_decoded || swr_get_delay(player->swr_context, 1)) {
		const uint8_t** in = (const uint8_t**)frame->extended_data;
		int in_size = frame->nb_samples;

		uint8_t* out[] = {audio_buffer->buffer + audio_buffer->data_size};
		int buffer_size = audio_buffer->capacity - audio_buffer->data_size;
		int buffer_size_in_frames = buffer_size / player->out_channels / player->bytes_per_sample;

		int converted_samples;

		if (!frame_decoded) {
			in = NULL;
			in_size = 0;
		}

		converted_samples = swr_convert(player->swr_context, out, buffer_size_in_frames, in, in_size);
		if (converted_samples < 0) {
			return -1;
		}

		*decoded_size = converted_samples * player->out_channels * player->bytes_per_sample;
		audio_buffer_update_after_write(audio_buffer, *decoded_size);
	}

	return data_size;
}


void handle_audio(Player* player) {
	AudioBuffer* audio_buffer = &player->audio_buffer;

	AVStream* stream = player->format_context->streams[player->audio_stream_index];
	double time_base = av_q2d(stream->time_base);

	static AVPacket packet;
	static AVPacket decode_packet;
	static int packet_size = 0;
	static int decoded_size = 0;

	int data_size;

	SDL_LockMutex(player->audio_mutex);
		while (audio_buffer->capacity - audio_buffer->data_size > AVCODEC_MAX_AUDIO_FRAME_SIZE) {
			if (packet_size > 0 || decoded_size > 0) {
				data_size = decode_audio(player, &decode_packet, &decoded_size);
				if (data_size < 0) {
					packet_size = 0;
				} else {
					player->audio_clock += (double)decoded_size / (double)player->bytes_per_second;

					decode_packet.data += data_size;
					decode_packet.size -= data_size;
					packet_size -= data_size;
				}

				if (packet_size > 0) {
					continue;
				}

				if (packet.data) {
					av_free_packet(&packet);
				}
			}

			if (get_audio_packet(player, &packet) < 0) {
				packet.data = NULL;
				packet.size = 0;

				if (decoded_size == 0) {
					break;
				}

			}

			decode_packet = packet;
			packet_size = packet.size;

			if (packet.pts != AV_NOPTS_VALUE) {
				player->audio_clock = packet.pts * time_base;
			}
		}
	SDL_UnlockMutex(player->audio_mutex);

	SDL_CondSignal(player->audio_cond);
}


double get_audio_clock(Player* player) {
	double audio_clock;
	double buffered_audio;

	SDL_LockMutex(player->audio_mutex);
		audio_clock = player->audio_clock;
		buffered_audio = (double)player->audio_buffer.data_size / (double)player->bytes_per_second;
	SDL_UnlockMutex(player->audio_mutex);

	return audio_clock - buffered_audio;
}


int update_video_clock_and_get_sleep_time(Player* player, double display_time) {
	int sleep_time;
	double audio_clock = get_audio_clock(player);

	player->video_clock = display_time;

	sleep_time = (int)rint((display_time - audio_clock) * 1000);
	if (sleep_time < 0) {
		return 0;
	}

	return sleep_time;
}


void display_video(Player* player, double display_time) {
	AVCodecContext* codec_context = player->video_codec_context;
	AVFrame* frame = player->video_frame;
	SDL_Overlay* overlay = player->overlay;
	struct SwsContext* sws_context = player->sws_context;

	int video_width = codec_context->width;
	int video_height = codec_context->height;

	SDL_Rect rect = {.x = 0, .y = 0, .w = video_width, .h = video_height};

	int sleep_time;

	uint8_t* data[3];
	int linesize[3];

	data[0] = overlay->pixels[0];
	data[1] = overlay->pixels[2];
	data[2] = overlay->pixels[1];

	linesize[0] = overlay->pitches[0];
	linesize[1] = overlay->pitches[2];
	linesize[2] = overlay->pitches[1];

	SDL_LockYUVOverlay(overlay);
		sws_scale(sws_context, (const uint8_t * const*)frame->data, frame->linesize, 0, video_height, data, linesize);
	SDL_UnlockYUVOverlay(overlay);

	sleep_time = update_video_clock_and_get_sleep_time(player, display_time);
	SDL_Delay(sleep_time);

	SDL_DisplayYUVOverlay(overlay, &rect);
}


void handle_video(Player* player) {
	AVCodecContext* codec_context = player->video_codec_context;
	AVFrame* frame = player->video_frame;
	int frame_decoded = 0;
	AVPacket packet;

	int64_t frame_timestamp;
	double display_time;

	AVStream* stream = player->format_context->streams[player->video_stream_index];
	double frame_duration = 1 / av_q2d(stream->avg_frame_rate);
	double time_base = av_q2d(stream->time_base);

	do {
		if (get_video_packet(player, &packet) < 0) {
			packet.data = NULL;
			packet.size = 0;
		}

		avcodec_decode_video2(codec_context, frame, &frame_decoded, &packet);
		if (frame_decoded) {
			if (frame->pkt_pts != AV_NOPTS_VALUE) {
				frame_timestamp = frame->pkt_pts;
			} else {
				if (frame->pkt_dts != AV_NOPTS_VALUE) {
					frame_timestamp = frame->pkt_dts;
				} else {
					frame_timestamp = packet.dts;
				}
			}

			if (frame_timestamp != AV_NOPTS_VALUE) {
				display_time = (double)frame_timestamp * time_base;
			} else {
				// approximate the time based on the previous value
				display_time = player->last_display_time + frame_duration;
			}

			player->last_display_time = display_time;
		}

		if (player->end_of_file_reached && !frame_decoded) {
			player->video_decoding_finished = 1;
			return;
		}

		if (packet.data) {
			av_free_packet(&packet);
		}
	} while (!frame_decoded);

	display_video(player, display_time);
}


void handle_events(Player* player) {
	SDL_Event event;

	while (SDL_PollEvent(&event)) {
		switch (event.type) {
			case SDL_QUIT:
				player->is_running = 0;
			default:
				break;
		}
	}
}


void check_end_of_file(Player* player) {
	if (player->end_of_file_reached) {
		if (player->video_decoding_finished) {
			player->is_running = 0;
		}
	}
}


void main_loop(Player* player) {
	while (player->is_running) {
		handle_audio(player);
		handle_video(player);
		handle_events(player);
		check_end_of_file(player);
	}
}


void audio_callback(void* p, Uint8* stream, int len) {
	Player* player = (Player*)p;
	AudioBuffer* audio_buffer = &player->audio_buffer;
	int data_size;

	SDL_LockMutex(player->audio_mutex);
		while (len > 0) {
			while (audio_buffer->data_size == 0 && player->is_running) {
				SDL_CondWait(player->audio_cond, player->audio_mutex);
			}

			data_size = audio_buffer->data_size;
			if (data_size > len) {
				data_size = len;
			}

			memcpy(stream, (uint8_t*)audio_buffer->buffer, data_size);
			len -= data_size;
			stream += data_size;

			audio_buffer_update_after_read(audio_buffer, data_size);
		}
	SDL_UnlockMutex(player->audio_mutex);
}


int main(int argc, char** argv) {
	int exit_code = 0;

	AVFormatContext* format_context = NULL;
	AVCodecContext* video_codec_context;
	AVCodecContext* audio_codec_context;
	AVCodec* video_codec;
	AVCodec* audio_codec;
	AVDictionary* video_codec_options = NULL;
	AVDictionary* audio_codec_options = NULL;
	AVFrame* frame;

	int video_stream_index;
	int audio_stream_index;
	int video_width;
	int video_height;

	SDL_Surface* surface;
	SDL_Overlay* overlay;

	SDL_AudioSpec request_spec;
	SDL_AudioSpec spec;

	struct SwsContext* sws_context;
	SwrContext* swr_context = NULL;
	uint64_t out_channel_layout;

	Player* player;

	if (argc < 2) {
		fprintf(stderr, "Please provide a multimedia file\n");
		exit_code = -1;
		goto terminate;
	}

	player = new_player();
	if (!player) {
		fprintf(stderr, "Failed to initialize the player\n");
		exit_code = -1;
		goto terminate;
	}

	av_register_all();

	if (avformat_open_input(&format_context, argv[1], NULL, NULL) != 0) {
		fprintf(stderr, "Failed to open input file\n");
		exit_code = -1;
		goto free_player;
	}
	player->format_context = format_context;

	if (avformat_find_stream_info(format_context, NULL) < 0) {
		fprintf(stderr, "Failed to find stream information\n");
		exit_code = -1;
		goto close_input;
	}

	video_stream_index = get_video_stream_index(format_context);
	if (video_stream_index < 0) {
		fprintf(stderr, "No video streams found\n");
		exit_code = -1;
		goto close_input;
	}
	video_codec_context = format_context->streams[video_stream_index]->codec;
	player->video_stream_index = video_stream_index;
	player->video_codec_context = video_codec_context;

	audio_stream_index = get_audio_stream_index(format_context);
	if (audio_stream_index < 0) {
		fprintf(stderr, "No audio streams found\n");
		exit_code = -1;
		goto close_input;
	}
	audio_codec_context = format_context->streams[audio_stream_index]->codec;
	player->audio_stream_index = audio_stream_index;
	player->audio_codec_context = audio_codec_context;

	dump_streams_info(format_context);

	video_width = video_codec_context->width;
	video_height = video_codec_context->height;

	video_codec = avcodec_find_decoder(video_codec_context->codec_id);
	if (!video_codec) {
		fprintf(stderr, "Unsupported video codec\n");
		exit_code = -1;
		goto close_input;
	}

	if (avcodec_open2(video_codec_context, video_codec, &video_codec_options) < 0) {
		fprintf(stderr, "Failed to open video codec\n");
		exit_code = -1;
		goto close_input;
	}
	player->video_codec = video_codec;

	audio_codec = avcodec_find_decoder(audio_codec_context->codec_id);
	if (!audio_codec) {
		fprintf(stderr, "Unsupported audio codec\n");
		exit_code = -1;
		goto close_video_codec;
	}

	if (avcodec_open2(audio_codec_context, audio_codec, &audio_codec_options) < 0) {
		fprintf(stderr, "Failed to open audio codec\n");
		exit_code = -1;
		goto close_video_codec;
	}
	player->audio_codec = audio_codec;

	frame = avcodec_alloc_frame();
	if (!frame) {
		fprintf(stderr, "Failed to allocate video frame\n");
		exit_code = -1;
		goto close_audio_codec;
	}
	player->video_frame = frame;

	frame = avcodec_alloc_frame();
	if (!frame) {
		fprintf(stderr, "Failed to allocate audio frame\n");
		exit_code = -1;
		goto free_video_frame;
	}
	player->audio_frame = frame;

	if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO)) {
		fprintf(stderr, "Failed to initialize SDL: %s\n", SDL_GetError());
		exit_code = -1;
		goto free_audio_frame;
	}

	surface = SDL_SetVideoMode(video_width, video_height, 0, 0);
	if (!surface) {
		fprintf(stderr, "Failed to set video mode: %s\n", SDL_GetError());
		exit_code = -1;
		goto close_sdl;
	}
	player->surface = surface;

	overlay = SDL_CreateYUVOverlay(video_width, video_height, SDL_YV12_OVERLAY, surface);
	if (!overlay) {
		fprintf(stderr, "Failed to create YUV overlay: %s\n", SDL_GetError());
		exit_code = -1;
		goto free_surface;
	}
	player->overlay = overlay;

	sws_context = sws_getContext(video_width, video_height, video_codec_context->pix_fmt, video_width, video_height, PIX_FMT_YUV420P, SWS_BILINEAR, NULL, NULL, NULL);
	if (!sws_context) {
		fprintf(stderr, "Failed to create swscale context\n");
		exit_code = -1;
		goto free_overlay;
	}
	player->sws_context = sws_context;

	request_spec.freq = audio_codec_context->sample_rate;
	request_spec.format = AUDIO_S16SYS;
	request_spec.channels = 2;
	request_spec.silence = 0;
	request_spec.samples = SDL_AUDIO_BUFFER_SIZE;
	request_spec.callback = audio_callback;
	request_spec.userdata = player;

	if (SDL_OpenAudio(&request_spec, &spec) < 0) {
		fprintf(stderr, "Failed to open audio: %s\n", SDL_GetError());
		exit_code = -1;
		goto free_sws_context;
	}

	out_channel_layout = av_get_default_channel_layout(spec.channels);
	swr_context = swr_alloc_set_opts(
		swr_context,
		out_channel_layout,
		AV_SAMPLE_FMT_S16,
		spec.freq,
		audio_codec_context->channel_layout,
		audio_codec_context->sample_fmt,
		audio_codec_context->sample_rate,
		0,
		NULL
	);
	if (!swr_context) {
		fprintf(stderr, "Failed to create swresample context\n");
		exit_code = -1;
		goto close_audio;
	}
	if (swr_init(swr_context) < 0) {
		fprintf(stderr, "Failed to initialize swresample context\n");
		exit_code = -1;
		goto free_swr_context;
	}
	player->swr_context = swr_context;
	player->out_channels = spec.channels;
	player->out_channel_layout = out_channel_layout;
	player->out_sample_fmt = AV_SAMPLE_FMT_S16;
	player->out_sample_rate = spec.freq;
	player->bytes_per_sample = av_get_bytes_per_sample(player->out_sample_fmt);
	player->bytes_per_second = player->out_channels * player->bytes_per_sample * player->out_sample_rate;

	player->is_running = 1;
	player->audio_clock = 0.0;
	player->video_clock = 0.0;
	player->end_of_file_reached = 0;
	player->audio_buffer.data_size = 0;
	player->video_decoding_finished = 0;
	SDL_PauseAudio(0);
	main_loop(player);
	SDL_CondSignal(player->audio_cond);

free_swr_context:
	swr_free(&swr_context);
close_audio:
	SDL_CloseAudio();
free_sws_context:
	sws_freeContext(sws_context);
free_overlay:
	SDL_FreeYUVOverlay(overlay);
free_surface:
	SDL_FreeSurface(surface);
close_sdl:
	SDL_Quit();
free_audio_frame:
	avcodec_free_frame(&player->audio_frame);
free_video_frame:
	avcodec_free_frame(&player->video_frame);
close_audio_codec:
	avcodec_close(audio_codec_context);
close_video_codec:
	avcodec_close(video_codec_context);
close_input:
	avformat_close_input(&format_context);
free_player:
	free_player(player);
terminate:
	return exit_code;
}
